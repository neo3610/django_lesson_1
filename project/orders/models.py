from django.contrib.auth.models import User
from django.db import models
from products.models import Product


class SalesOrder(models.Model):
    amount = models.IntegerField()
    description = models.CharField(max_length=255)
    #user = models.ForeignKey(User, on_delete=models.SET_NULL()) #проставит ноль
    # user = models.ForeignKey(User, on_delete=models.PROTECT())  # не позволит удалить пока есть значения
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True) #каскадное удаление
    products = models.ManyToManyField(Product)
    #account = models.OneToOneField(Product) #!!!!!!!!!!!!

